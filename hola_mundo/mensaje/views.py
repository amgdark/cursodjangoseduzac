from django.shortcuts import render

# Create your views here.
def funcion_hola(request):
    context = {
        'nombre': 'Alex',
        'edad': 41,
        'materias':[
            'Linux',
            'Testing',
            'Deployment',
            'Admon. proyectos',
            'Frameworks',
            'PSP',
            'TSP'
        ]
    }
    return render(request, 'mensaje.html', context )