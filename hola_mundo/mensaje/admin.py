from django.contrib import admin
from mensaje.models import Alumno, Materia, CentroTrabajo

# Register your models here.

admin.site.register(Alumno)
admin.site.register(Materia)
admin.site.register(CentroTrabajo)