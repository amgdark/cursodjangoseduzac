import code
from django.db import models
from mensaje import validadores

GENERO = [
    ('F','Femenino'),
    ('M','Masculino'),
]

class Alumno(models.Model):
    curp = models.CharField('C.U.R.P.', max_length=18, unique=True, validators=[validadores.curp_validador])
    nombre = models.CharField(max_length=100)
    apellido_paterno = models.CharField(max_length=100)
    apellido_materno = models.CharField(max_length=100)
    telefono = models.CharField('Teléfono', max_length=10, null=True, blank=True)
    edad = models.IntegerField()
    genero = models.CharField(max_length=1, choices=GENERO)
    email = models.EmailField('Correo electrónico', max_length=254)
    fecha_nacimiento = models.DateField("Fecha de nacimiento")
    cct = models.ForeignKey("mensaje.CentroTrabajo", verbose_name='CCT', on_delete=models.DO_NOTHING)
    materias = models.ManyToManyField("mensaje.Materia", verbose_name='Materias')
    
    def __str__(self):
        return self.curp 
    
class Materia(models.Model):
    nombre = models.CharField(max_length=50)
    
    def __str__(self):
        return self.nombre
    
class CentroTrabajo(models.Model):
    cct = models.CharField(max_length=10, unique=True, primary_key=True)
    nombre = models.CharField(max_length=150)
    
    def __str__(self):
        return self.cct