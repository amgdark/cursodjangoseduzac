from django.urls import path
from usuarios import views
from django.contrib.auth.views import LogoutView


urlpatterns = [
    path('login', views.Login.as_view(), name='login' ),
    path('logout', LogoutView.as_view(), name='logout' ),
    # path('nuevo', views.nuevo_alumno, name='nuevo_alumno'),
    # path('editar/<int:id>', views.editar_alumno, name='editar_alumno'),
    # path('eliminar/<int:id>', views.eliminar_alumno, name='eliminar_alumno'),
    
]