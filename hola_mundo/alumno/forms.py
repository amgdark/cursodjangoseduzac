from dataclasses import fields
from pyexpat import model
from django import forms
from mensaje.models import Alumno


class FormAlumno(forms.ModelForm):
    
    class Meta:
        model = Alumno
        fields = '__all__'
        # fields = ['edad','nombre','apellido_paterno']
        # exclude = ['curp']
        
        widgets = {
            'curp': forms.TextInput(attrs={'class': 'form-control'}),
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'apellido_paterno': forms.TextInput(attrs={'class': 'form-control'}),
            'apellido_materno': forms.TextInput(attrs={'class': 'form-control'}),
            'edad': forms.TextInput(attrs={'class': 'form-control'}),
        }

