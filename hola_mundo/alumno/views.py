from django.shortcuts import render, redirect
from mensaje.models import Alumno
from alumno.forms import FormAlumno
from django.contrib.auth.decorators import login_required


@login_required
def lista_alumnos(request):
    # alumnos = Alumno.objects.all()
    # alumnos = Alumno.objects.get(nombre='Alex')
    alumnos = Alumno.objects.filter(
        materias__nombre='Computación')
    
    print (alumnos.query)
    return render(request, 'alumnos.html', {'alumnos': alumnos})

@login_required
def nuevo_alumno(request):
    if request.method == 'POST':
        form = FormAlumno(request.POST)
        if form.is_valid():
            form.save()
            return redirect('lista_alumnos')
    else:
        form = FormAlumno()
                    
    return render(request, 'nuevo_alumno.html',{'form':form})

def editar_alumno(request, id):
    alumno = Alumno.objects.get(id = id)
    if request.method == 'POST':
        form = FormAlumno(request.POST, instance=alumno)
        if form.is_valid():
            form.save()
            return redirect('lista_alumnos')
    else:
        form = FormAlumno(instance=alumno)
                    
    return render(request, 'editar_alumno.html',{'form':form})

def eliminar_alumno(request, id):
    Alumno.objects.get(id = id).delete()
    return redirect('lista_alumnos')
    
    