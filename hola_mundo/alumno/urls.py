from unicodedata import name
from django.urls import path
from alumno import views


urlpatterns = [
    path('', views.lista_alumnos, name='lista_alumnos' ),
    path('nuevo', views.nuevo_alumno, name='nuevo_alumno'),
    path('editar/<int:id>', views.editar_alumno, name='editar_alumno'),
    path('eliminar/<int:id>', views.eliminar_alumno, name='eliminar_alumno'),
    # path('agregar-ct', ),
    
]

# localhost:8000/alumnos
# localhost:8000/alumnos/nuevo
# localhost:8000/alumnos/editar
# localhost:8000/alumnos/agregar-ct