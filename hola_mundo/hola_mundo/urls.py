from django.contrib import admin
from django.urls import path, include
from mensaje.views import funcion_hola


urlpatterns = [
    path('admin/', admin.site.urls),
    path('hola/', funcion_hola),
    path('alumnos/', include('alumno.urls') ),
    path('', include('usuarios.urls') ),
    
]

# localhost:8000/alumnos