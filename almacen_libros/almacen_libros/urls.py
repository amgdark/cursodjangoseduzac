
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('libros/', include('libros.urls')),
    path('', TemplateView.as_view(template_name="bienvenida.html"), name='bienvenida'),
    path('', include('usuarios.urls') ),
    path('ccts/', include('ccts.urls')),

]
