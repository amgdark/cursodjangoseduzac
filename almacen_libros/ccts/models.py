from django.db import models



class CentroTrabajo(models.Model):
    nombre = models.CharField(max_length=100)
    cct = models.CharField(max_length=100)
    latitud = models.CharField(max_length=50)
    longitud = models.CharField(max_length=50)
