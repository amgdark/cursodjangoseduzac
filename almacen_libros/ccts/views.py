from django.shortcuts import render
from django.views.generic.edit import CreateView
from django.views.generic import ListView, DetailView
from ccts.forms import FormCentroTrabajo
from ccts.models import CentroTrabajo
from django.urls import reverse_lazy


class CrearCentroTrabajoView(CreateView):
    model = CentroTrabajo
    form_class = FormCentroTrabajo
    # success_url = reverse_lazy('ccts:detalle', pk=)

    def get_success_url(self):
        return reverse_lazy('ccts:detalle',args=(self.object.id,))


class ListaCentroTrabajoView(ListView):
    model = CentroTrabajo


class DetalleCentroTrabajoView(DetailView):
    model = CentroTrabajo