from django.apps import AppConfig


class CctsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ccts'
