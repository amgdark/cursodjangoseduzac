from django import forms
from ccts.models import CentroTrabajo

class FormCentroTrabajo(forms.ModelForm):

    class Meta:
        model = CentroTrabajo
        fields = '__all__'

        widgets = {
            # 'latitud': forms.HiddenInput(),
            # 'longitud': forms.HiddenInput(),
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'cct': forms.TextInput(attrs={'class':'form-control'}),
            'latitud': forms.TextInput(attrs={'readonly':'true', 'class':'form-control'}),
            'longitud': forms.TextInput(attrs={'readonly':'true', 'class':'form-control'}),

        }