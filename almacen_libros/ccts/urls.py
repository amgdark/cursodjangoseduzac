from django.urls import path
from ccts import views


app_name = 'ccts'

urlpatterns = [
    path('nuevo', views.CrearCentroTrabajoView.as_view(), name='nuevo' ),
    path('', views.ListaCentroTrabajoView.as_view(), name='lista' ),
    path('<int:pk>', views.DetalleCentroTrabajoView.as_view(), name='detalle' ),
]
