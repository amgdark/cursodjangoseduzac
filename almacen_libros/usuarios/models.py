from pyexpat import model
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class DatosUsuario(models.Model):
    curp = models.CharField(max_length=18)
    foto = models.ImageField("Avatar", upload_to='avatares')
    usuario = models.OneToOneField(User, verbose_name="Usuario", on_delete=models.DO_NOTHING)