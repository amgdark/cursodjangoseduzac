from django.shortcuts import render
from libros.models import AlLibro
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.contrib import messages


def lista_libros(request):
    libros = AlLibro.objects.all()
    return render(request, 'lista_libros.html', {'libros':libros})


class ListaLibroView(ListView):
    model = AlLibro
    # template_name = 'mihtml.html'
    # context_object_name  = 'libros'
    queryset = AlLibro.objects.filter(estatus='A')
    ordering = ('-titulo')
    extra_context = {'nombre':'Alex'}
    paginate_by = 10

class CrearLibroView(CreateView):
    model = AlLibro
    fields = '__all__'
    #form_class = FormLibro
    extra_context = {'tipo':'Nuevo'}
    success_url = reverse_lazy('libros:lista')

class EditarLibroView(UpdateView):
    model = AlLibro
    fields = '__all__'
    #form_class = FormLibro
    extra_context = {'tipo':'Actualizar'}
    success_url = reverse_lazy('libros:lista')


class EliminarLibroView(DeleteView):
    model = AlLibro
    success_url = reverse_lazy('libros:lista')

    def form_valid(self, form):
        success_url = self.get_success_url()
        try:
            self.object.delete()
            messages.success(self.request, f"Se eliminó el libro {self.object} de manera exitosa.")

        except:
            messages.error(self.request, f"No se puede eliminar el libro {self.object}.")
        return HttpResponseRedirect(success_url)