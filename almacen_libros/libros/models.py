from django.db import models


class AlDetalleEntrada(models.Model):
    id_detalle_entrada = models.IntegerField(db_column='ID_DETALLE_ENTRADA', primary_key=True)
    id_entrada = models.ForeignKey('AlEntrada', models.DO_NOTHING, db_column='ID_ENTRADA')
    id_libro = models.ForeignKey('AlLibro', models.DO_NOTHING, db_column='ID_LIBRO')
    cantidad = models.IntegerField(db_column='CANTIDAD')

    class Meta:
        db_table = 'AL_DETALLE_ENTRADA'


class AlDetalleSalida(models.Model):
    id_detalle_salida = models.IntegerField(db_column='ID_DETALLE_SALIDA', primary_key=True)
    id_salida = models.ForeignKey('AlSalida', models.DO_NOTHING, db_column='ID_SALIDA')
    id_libro = models.ForeignKey('AlLibro', models.DO_NOTHING, db_column='ID_LIBRO')
    cantidad = models.IntegerField(db_column='CANTIDAD')
    unidades_caja = models.SmallIntegerField(db_column='UNIDADES_CAJA')

    class Meta:
        db_table = 'AL_DETALLE_SALIDA'


class AlDistribucion(models.Model):
    id_distribucion = models.SmallIntegerField(db_column='ID_DISTRIBUCION', primary_key=True)
    clave_escuela = models.CharField(db_column='CLAVE_ESCUELA', max_length=11)
    nombre_escuela = models.CharField(db_column='NOMBRE_ESCUELA', max_length=150)
    localidad_escuela = models.CharField(db_column='LOCALIDAD_ESCUELA', max_length=100, blank=True, null=True)
    municipio_escuela = models.CharField(db_column='MUNICIPIO_ESCUELA', max_length=100, blank=True, null=True)
    clave_region = models.CharField(db_column='CLAVE_REGION', max_length=11)
    nombre_region = models.CharField(db_column='NOMBRE_REGION', max_length=100)
    clave_supervision = models.CharField(db_column='CLAVE_SUPERVISION', max_length=15, blank=True, null=True)
    nombre_supervision = models.CharField(db_column='NOMBRE_SUPERVISION', max_length=100, blank=True, null=True)
    alumnos_primero = models.SmallIntegerField(db_column='ALUMNOS_PRIMERO')
    alumnos_segundo = models.SmallIntegerField(db_column='ALUMNOS_SEGUNDO')
    alumnos_tercero = models.SmallIntegerField(db_column='ALUMNOS_TERCERO')
    alumnos_cuarto = models.SmallIntegerField(db_column='ALUMNOS_CUARTO')
    alumnos_quinto = models.SmallIntegerField(db_column='ALUMNOS_QUINTO')
    alumnos_sexto = models.SmallIntegerField(db_column='ALUMNOS_SEXTO')
    total_alumnos = models.IntegerField(db_column='TOTAL_ALUMNOS')
    nivel_educativo = models.CharField(db_column='NIVEL_EDUCATIVO', max_length=20)
    estatus_escuela = models.CharField(db_column='ESTATUS_ESCUELA', max_length=20)
    estatus_registro = models.CharField(db_column='ESTATUS_REGISTRO', max_length=20)
    observaciones = models.CharField(db_column='OBSERVACIONES', max_length=200, blank=True, null=True)
    ciclo_escolar = models.CharField(db_column='CICLO_ESCOLAR', max_length=10)
    fecha = models.DateField()

    class Meta:
        db_table = 'AL_DISTRIBUCION'


class AlEntrada(models.Model):
    id_entrada = models.IntegerField(db_column='ID_ENTRADA', primary_key=True)
    folio = models.IntegerField(db_column='FOLIO', blank=True, null=True)
    fecha = models.DateTimeField(db_column='FECHA', blank=True, null=True)
    factura = models.CharField(db_column='FACTURA', max_length=30)
    cve_ct = models.CharField(db_column='CVE_CT', max_length=10)
    estatus = models.SmallIntegerField(db_column='ESTATUS')
    observ = models.TextField(db_column='OBSERV', blank=True, null=True)
    id_usuario = models.IntegerField(db_column='ID_USUARIO', blank=True, null=True)
    ciclo_escolar = models.CharField(db_column='CICLO_ESCOLAR', max_length=10)
    id_salida = models.ForeignKey('AlSalida', models.DO_NOTHING, db_column='ID_SALIDA', blank=True, null=True)

    class Meta:
        db_table = 'AL_ENTRADA'


class AlEstadisticaNivel(models.Model):
    id = models.IntegerField(db_column='ID', primary_key=True)
    cct_escuela = models.CharField(db_column='CCT_ESCUELA', max_length=20, blank=True, null=True)
    cct_region = models.CharField(db_column='CCT_REGION', max_length=20, blank=True, null=True)
    cct_supervision = models.CharField(db_column='CCT_SUPERVISION', max_length=20, blank=True, null=True)
    alumnos_grado1 = models.IntegerField(db_column='ALUMNOS_GRADO1', blank=True, null=True)
    alumnos_grado2 = models.IntegerField(db_column='ALUMNOS_GRADO2', blank=True, null=True)
    alumnos_grado3 = models.IntegerField(db_column='ALUMNOS_GRADO3', blank=True, null=True)
    alumnos_grado4 = models.IntegerField(db_column='ALUMNOS_GRADO4', blank=True, null=True)
    alumnos_grado5 = models.IntegerField(db_column='ALUMNOS_GRADO5', blank=True, null=True)
    alumnos_grado6 = models.IntegerField(db_column='ALUMNOS_GRADO6', blank=True, null=True)
    grupos_grado1 = models.IntegerField(db_column='GRUPOS_GRADO1', blank=True, null=True)
    grupos_grado2 = models.IntegerField(db_column='GRUPOS_GRADO2', blank=True, null=True)
    grupos_grado3 = models.IntegerField(db_column='GRUPOS_GRADO3', blank=True, null=True)
    grupos_grado4 = models.IntegerField(db_column='GRUPOS_GRADO4', blank=True, null=True)
    grupos_grado5 = models.IntegerField(db_column='GRUPOS_GRADO5', blank=True, null=True)
    grupos_grado6 = models.IntegerField(db_column='GRUPOS_GRADO6', blank=True, null=True)
    nivel = models.CharField(db_column='NIVEL', max_length=20, blank=True, null=True)
    total_grupos = models.IntegerField(db_column='TOTAL_GRUPOS', blank=True, null=True)
    total_docentes = models.IntegerField(db_column='TOTAL_DOCENTES', blank=True, null=True)

    class Meta:
        db_table = 'AL_ESTADISTICA_NIVEL'


class AlInventario(models.Model):
    id_inventario = models.IntegerField(db_column='ID_INVENTARIO', primary_key=True)
    id_libro = models.ForeignKey('AlLibro', models.DO_NOTHING, db_column='ID_LIBRO')
    fecha = models.DateTimeField(db_column='FECHA')
    cantidad = models.IntegerField(db_column='CANTIDAD')
    cve_ct = models.CharField(db_column='CVE_CT', max_length=10)

    class Meta:
        db_table = 'AL_INVENTARIO'


class AlLibro(models.Model):
    id_libro = models.IntegerField(db_column='ID_LIBRO', primary_key=True)
    cve_programa = models.ForeignKey('AlPrograma', models.DO_NOTHING, verbose_name='Clave', db_column='CVE_PROGRAMA', blank=True, null=True)
    titulo = models.CharField('Título', db_column='TITULO', max_length=150)
    grado = models.SmallIntegerField(db_column='GRADO', blank=True, null=True)
    clave = models.CharField(db_column='CLAVE', max_length=100)
    unidades_caja = models.SmallIntegerField(db_column='UNIDADES_CAJA')
    estatus = models.CharField(db_column='ESTATUS', max_length=1)
    id_nivel = models.ForeignKey('AlNivel', models.DO_NOTHING, verbose_name='Nivel', db_column='ID_NIVEL', blank=True, null=True)
    dirigidoa = models.SmallIntegerField('Dirigido a:', db_column='DIRIGIDOA')

    def __str__(self):
        return self.titulo
    

    class Meta:
        db_table = 'AL_LIBRO'
        unique_together = (('clave', 'cve_programa'),)


class AlLibrosSecundaria(models.Model):
    id_libros_secundaria = models.IntegerField(db_column='ID_LIBROS_SECUNDARIA', primary_key=True)
    id_libro = models.IntegerField(db_column='ID_LIBRO', blank=True, null=True)
    tipo_pedido = models.CharField(db_column='TIPO_PEDIDO', max_length=10, blank=True, null=True)
    municipio = models.CharField(db_column='MUNICIPIO', max_length=100, blank=True, null=True)
    clave_region = models.CharField(db_column='CLAVE_REGION', max_length=10, blank=True, null=True)
    nombre_region = models.CharField(db_column='NOMBRE_REGION', max_length=100, blank=True, null=True)
    clave_supervision = models.CharField(db_column='CLAVE_SUPERVISION', max_length=10, blank=True, null=True)
    clave_escuela = models.CharField(db_column='CLAVE_ESCUELA', max_length=10)
    nombre_escuela = models.CharField(db_column='NOMBRE_ESCUELA', max_length=100)
    turno = models.CharField(db_column='TURNO', max_length=30)
    grado = models.CharField(db_column='GRADO', max_length=2)
    grupo = models.CharField(db_column='GRUPO', max_length=2)
    materia = models.CharField(db_column='MATERIA', max_length=100, blank=True, null=True)
    clave_libro = models.CharField(db_column='CLAVE_LIBRO', max_length=10, blank=True, null=True)
    titulo_libro = models.CharField(db_column='TITULO_LIBRO', max_length=150, blank=True, null=True)
    editorial = models.CharField(db_column='EDITORIAL', max_length=150, blank=True, null=True)
    autor = models.CharField(db_column='AUTOR', max_length=250, blank=True, null=True)
    cantidad_libros = models.SmallIntegerField(db_column='CANTIDAD_LIBROS', blank=True, null=True)
    nombre_almacen = models.CharField(db_column='NOMBRE_ALMACEN', max_length=50, blank=True, null=True)
    ciclo_escolar = models.CharField(db_column='CICLO_ESCOLAR', max_length=10)

    class Meta:
        db_table = 'AL_LIBROS_SECUNDARIA'


class AlNivel(models.Model):
    id_nivel = models.SmallIntegerField(db_column='ID_NIVEL', primary_key=True)
    nivel = models.CharField(db_column='NIVEL', max_length=50)
    estatus = models.SmallIntegerField(db_column='ESTATUS')

    def __str__(self):
        return self.nivel

    class Meta:
        db_table = 'AL_NIVEL'


class AlPaqueteLibros(models.Model):
    id_paq_libros = models.SmallIntegerField(db_column='ID_PAQ_LIBROS', primary_key=True)
    id_libro = models.IntegerField(db_column='ID_LIBRO', blank=True, null=True)
    clave_libro = models.CharField(db_column='CLAVE_LIBRO', max_length=20, blank=True, null=True)
    titulo_libro = models.CharField(db_column='TITULO_LIBRO', max_length=150)
    grado = models.CharField(db_column='GRADO', max_length=10)
    nivel_educativo = models.CharField(db_column='NIVEL_EDUCATIVO', max_length=30, blank=True, null=True)
    ciclo_escolar = models.CharField(db_column='CICLO_ESCOLAR', max_length=30, blank=True, null=True)
    estatus_registro = models.CharField(db_column='ESTATUS_REGISTRO', max_length=20)
    alias = models.CharField(db_column='ALIAS', max_length=20, blank=True, null=True)

    class Meta:
        db_table = 'AL_PAQUETE_LIBROS'


class AlPautaLibros(models.Model):
    id_pauta = models.IntegerField(db_column='ID_PAUTA', primary_key=True)
    id_libro = models.IntegerField(db_column='ID_LIBRO', blank=True, null=True)
    clave_libro = models.CharField(db_column='CLAVE_LIBRO', max_length=12)
    titulo_libro = models.CharField(db_column='TITULO_LIBRO', max_length=150, blank=True, null=True)
    grado = models.CharField(db_column='GRADO', max_length=10, blank=True, null=True)
    total = models.IntegerField(db_column='TOTAL')
    servicio_educativo = models.CharField(db_column='SERVICIO_EDUCATIVO', max_length=20)
    almacen = models.CharField(db_column='ALMACEN', max_length=50, blank=True, null=True)
    ciclo_escolar = models.CharField(db_column='CICLO_ESCOLAR', max_length=10, blank=True, null=True)

    class Meta:
        db_table = 'AL_PAUTA_LIBROS'


class AlPrograma(models.Model):
    cve_programa = models.IntegerField(db_column='CVE_PROGRAMA', primary_key=True)
    nombre = models.CharField(db_column='NOMBRE', max_length=100)
    descripcion = models.CharField(db_column='DESCRIPCION', max_length=100)
    cve = models.CharField(db_column='CVE', max_length=10, blank=True, null=True)
    estatus = models.CharField(db_column='ESTATUS', max_length=1)

    def __str__(self):
        return str(self.cve_programa) + " " + self.nombre


    class Meta:
        db_table = 'AL_PROGRAMA'


class AlSalida(models.Model):
    id_salida = models.IntegerField(db_column='ID_SALIDA', primary_key=True)
    folio = models.IntegerField(db_column='FOLIO', blank=True, null=True)
    cve_ct_origen = models.CharField(db_column='CVE_CT_ORIGEN', max_length=10)
    cve_ct_destino = models.CharField(db_column='CVE_CT_DESTINO', max_length=10)
    fecha = models.DateTimeField(db_column='FECHA', blank=True, null=True)
    recibe = models.CharField(db_column='RECIBE', max_length=100, blank=True, null=True)
    observa = models.TextField(db_column='OBSERVA', blank=True, null=True)
    estatus = models.SmallIntegerField(db_column='ESTATUS')
    id_usuario = models.IntegerField(db_column='ID_USUARIO', blank=True, null=True)
    ciclo_escolar = models.CharField(db_column='CICLO_ESCOLAR', max_length=10)

    class Meta:
        db_table = 'AL_SALIDA'


class AlTemp(models.Model):
    clave = models.CharField(db_column='CLAVE', max_length=20, blank=True, null=True)
    nombre = models.CharField(db_column='NOMBRE', max_length=80, blank=True, null=True)
    cantidad = models.IntegerField(db_column='CANTIDAD', blank=True, null=True)
    grado = models.IntegerField(db_column='GRADO', blank=True, null=True)
    nivel = models.CharField(db_column='NIVEL', max_length=40, blank=True, null=True)

    class Meta:
        db_table = 'AL_TEMP'

