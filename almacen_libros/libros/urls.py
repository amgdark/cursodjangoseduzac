
from django.urls import path
from libros import views

app_name = 'libros'

urlpatterns = [
    path('lista', views.lista_libros),
    path('lista2', views.ListaLibroView.as_view(), name='lista'),
    path('nuevo', views.CrearLibroView.as_view(), name='nuevo'),
    path('editar/<int:pk>', views.EditarLibroView.as_view(), name='editar'),
    path('eliminar/<int:pk>', views.EliminarLibroView.as_view(), name='eliminar'),

]
