from behave import given, when, then
from calculadora import Calculadora


@given(u'que ingreso los numeros "{num1}" y "{num2}"')
def step_impl(context, num1, num2):
    context.calc = Calculadora()
    context.num1 = int(num1)
    context.num2 = int(num2)

@given(u'que ingreso el numero decimal "{num1}" y el entero "{num2}"')
def step_impl(context, num1, num2):
    context.calc = Calculadora()
    context.num1 = float(num1)
    context.num2 = int(num2)

@when(u'realizo la suma')
def step_impl(context):
    context.resultado = context.calc.sumar(context.num1, context.num2)


@then(u'puedo ver el resultado que es "{esperado}"')
def step_impl(context, esperado):
    assert context.resultado == int(esperado), \
        f"El resultado es {str(context.resultado)} y el esperado es {esperado}"

@then(u'puedo ver el resultado en decimal "{esperado}"')
def step_impl(context, esperado):
    assert context.resultado == float(esperado), \
        f"El resultado es {str(context.resultado)} y el esperado es {esperado}"


@given(u'que ingreso el caracter "{caracter}" y el entero "{num2}"')
def step_impl(context, caracter, num2):
    context.calc = Calculadora()
    context.num1 = caracter
    context.num2 = int(num2)

@then(u'puedo ver el mensaje "{esperado}"')
def step_impl(context, esperado):
    assert context.resultado == esperado, \
        f"El resultado es {str(context.resultado)} y el esperado es {esperado}"
