Característica: Como usuario de la aplicación calculadora quiero sumar dos elementos para conocer su resultado

    Escenario: Sumar dos mas dos
        Dado que ingreso los numeros "2" y "2"
        Cuando realizo la suma
        Entonces puedo ver el resultado que es "4"

    Escenario: Sumar 4 mas 8
        Dado que ingreso los numeros "4" y "8"
        Cuando realizo la suma
        Entonces puedo ver el resultado que es "12"

    Escenario: Sumar -4 mas 5
        Dado que ingreso los numeros "-4" y "5"
        Cuando realizo la suma
        Entonces puedo ver el resultado que es "1"

    Escenario: Sumar 20.5 mas 50
        Dado que ingreso el numero decimal "20.5" y el entero "50"
        Cuando realizo la suma
        Entonces puedo ver el resultado en decimal "70.5"

    Escenario: Sumar X mas 30
        Dado que ingreso el caracter "X" y el entero "30"
        Cuando realizo la suma
        Entonces puedo ver el mensaje "Sólo se admiten números"